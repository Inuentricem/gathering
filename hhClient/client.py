import logging
import requests
import json

logger = logging.getLogger(__name__)


class HHClient(object):
    COUNT_PER_PAGE = 10
    URL = "https://api.hh.ru/vacancies/"

    def __init__(self, token, skip_objects=None):
        self.skip_objects = skip_objects
        self.token = token

    def process(self, query, storage):

        vacancies_id_list = HHClient.__get_vacancies_id(query, 15)
        for vacancy_id in vacancies_id_list:
            storage.append_data(HHClient.__extract_vacancy_data(vacancy_id))
        self.token = 1
        logger.info(vacancies_id_list)

    @staticmethod
    def __extract_vacancy_data(vacancy_id):
        vacancy_description = HHClient.__get_vacancy_description(vacancy_id)

        salary_from = vacancy_description["salary"]["from"] if type(vacancy_description["salary"]["from"]) == "NoneType" else 0
        salary_to = vacancy_description["salary"]["to"] if type(vacancy_description["salary"]["to"]) == "NoneType" else 0

        data = vacancy_description["experience"]["id"] + ',' \
            + str(salary_from) + ',' \
            + str(salary_to)

        return data

    @staticmethod
    def __get_vacancy_description(vacancy_id):

        response = requests.get(HHClient.URL+vacancy_id)

        if not response.ok:
            logger.error(response.text)
            # then continue process, or retry, or fix your code

        else:
            # Note: here json can be used as response.json
            data = response.text
            vacancy_description = json.loads(data)

        return vacancy_description

    @staticmethod
    def __get_vacancies_id(query, count=100):
        vacancies_id_list = []
        max_page = round(count/HHClient.COUNT_PER_PAGE)

        for page in range(max_page):
            params = {
               'text': query,
               'per_page': HHClient.COUNT_PER_PAGE,
               'page': page
            }

            response = requests.get(HHClient.URL, params=params)

            if not response.ok:
                logger.error(response.text)
                # then continue process, or retry, or fix your code

            else:
                # Note: here json can be used as response.json
                data = response.text
                current_vacancies_list = json.loads(data)
                for short_vacancy_description in current_vacancies_list["items"]:
                    vacancies_id_list.append(short_vacancy_description["id"])

        return vacancies_id_list
